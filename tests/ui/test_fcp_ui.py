import allure


@allure.severity('medium')
@allure.testcase('https://app.qase.io/case/FCP/edit/7')
def test_forgot_password_ui(driver):
    driver.get('https://dev-front-office.deploy.guru/')
    assert driver.title == 'React Index'


@allure.severity('blocker')
@allure.testcase('https://app.qase.io/case/FCP/edit/5')
def test_client_login_ui(driver):
    driver.get('https://dev-front-office.deploy.guru/login')
    assert driver.title == 'React Index'


@allure.severity('blocker')
@allure.testcase('https://app.qase.io/case/FCP/edit/5')
def test_google_ui(driver):
    driver.get('https://www.google.com/')
    assert driver.title == 'Google'
