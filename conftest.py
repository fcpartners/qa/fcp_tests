import pytest

from pathlib import Path
from selenium import webdriver


@pytest.fixture()
def get_active_branch_name():
    head_dir = Path(".") / ".git" / "HEAD"
    with head_dir.open("r") as f:
        content = f.read().splitlines()

    for line in content:
        if line[0:4] == "ref:":
            return line.partition("refs/heads/")[2]


@pytest.fixture(scope="function")
def driver(get_active_branch_name):
    if get_active_branch_name is None:
        driver = webdriver.Remote(command_executor="http://selenium__standalone-chrome:4444/wd/hub")
    else:
        driver = webdriver.Chrome()
    yield driver
    driver.quit()
